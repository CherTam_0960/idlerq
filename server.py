import socket
import struct

# ประกาศตัวแปรสถานะต่างๆของ ACK
IDLE = 0
BUSY = 1
ACK = 2
NAK = 3

# สร้าง Socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# กำหนดเลข port ให้กับ socket
server_socket.bind(('127.0.0.1', 5000))

# รอการเชื่อมต่อจากฝั่ง client
server_socket.listen(1)

# ตอบรับการเชื่อมต่อจาก client
client_socket, client_address = server_socket.accept()

# รับชื่อไฟล์ที่ต้องการ ในที่นี้คือ "input.txt"
filename = client_socket.recv(1024).decode()

# เปิดไฟล์บนเครื่อง server
file = open(filename, 'rb')

# ส่งข้อมูลไฟล์เป็นส่วนๆ ขนาด 1024 ไบต์
while True:
    data = file.read(1024)
    if not data:
        break

    # ส่ง packet ข้อมูล
    packet = struct.pack('!ii', IDLE, len(data)) + data
    client_socket.send(packet)

    # รับการยืนยัน ACK
    ack = client_socket.recv(4)

    # ตรวจสอบความยาวของการยืนยัน
    if len(ack) != 4:
        # การยืนยันเสียหาย
        # ละทิ้งแพ็กเก็ตและรอแพ็กเก็ตใหม่
        print("การยืนยันเสียหาย")
        continue
    # แกะแพ็กเก็ตการยืนยัน
    ack_type = struct.unpack('!i', ack)[0]

    if ack_type == ACK:
        # แพ็กเก็ตข้อมูลได้รับถูกต้อง
        print("แพ็กเก็ตข้อมูลได้รับถูกต้อง")
        continue
    elif ack_type == NAK:
        # แพ็กเก็ตข้อมูลไม่ได้รับอย่างถูกต้อง
        # ส่งแพ็กเก็ตข้อมูลอีกครั้ง
        print("แพ็กเก็ตข้อมูลไม่ได้รับอย่างถูกต้อง")
        client_socket.send(packet)
    else:
        # ประเภทการยืนยันที่ไม่คาดคิด
        break

# ปิดไฟล์ ในที่นี้คือ "input.txt"
file.close()

# ปิด client socket
client_socket.close()

# ปิด server socket
server_socket.close()
