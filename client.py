import socket
import struct

# ประกาศตัวแปรสถานะต่างๆของ ACK
IDLE = 0
BUSY = 1
ACK = 2
NAK = 3

# สร้าง Socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# กำหนดเลข port ให้กับ socket
client_socket.connect(('127.0.0.1', 5000))

# ส่งชื่อไฟล์
client_socket.send('input.txt'.encode())

# สร้างไฟล์เพื่อรับข้อมูล ในที่นี้คือ 'received_text.txt'
file = open('received_text.txt', 'w')

while True:
    # รับแพ็กเก็ตข้อมูล (ขนาดแพ็กเก็ตสูงสุดคือ 1024 ไบต์)
    packet = client_socket.recv(1024)

    # ตรวจสอบว่าแพ็กเก็ตว่างเปล่าหรือไม่
    if not packet:
        break

    # แกะแพ็กเก็ตข้อมูลส่วนหัว (4 ไบต์สำหรับ packet_type ส่วนที่เหลือสำหรับข้อมูล)
    packet_type, data = struct.unpack('!i', packet[:4]), packet[4:]

    if packet_type[0] == IDLE:
        # เขียนข้อมูลไปยังไฟล์
        file.write(data.decode())
        print("packet_type = IDLE")
        # ส่งการยืนยัน
        ack = struct.pack('!i', ACK)
        client_socket.send(ack)
    elif packet_type[0] == BUSY:
        # การยืนยันไม่ได้รับอย่างถูกต้อง
        # ส่งการยืนยันอีกครั้ง
        continue
    elif packet_type[0] == NAK:
        # การยืนยันไม่ได้รับอย่างถูกต้อง
        # ส่งการยืนยันอีกครั้ง
        print("packet_type = NAK")
        client_socket.send(ack)
    else:
        # ประเภทการยืนยันที่ไม่คาดคิด
        break

# ปิดไฟล์ ในที่นี้คือ "received_text.txt"
file.close()

# ปิด client socket
client_socket.close()
